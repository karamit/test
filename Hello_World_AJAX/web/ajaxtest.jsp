<%-- 
    Document   : ajaxtest
    Created on : 22 Oct, 2016, 6:40:02 AM
    Author     : pc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1
        <p>Radii</p> <input type="text" id="radii"><br>
        <p>Circumference</p> <input type="text" id="circum"><br>
        <button id="calc" onclick="calccircum()">Calculate</button>
        <button id="reset">reset</button><br>
        <div id="result"></div>
    </body>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>
        
//        $(document).ready(function(){
//            alert("jquery is working...");
//        })
        
//        function calcarea(){
//            var radius = $('#radii').val();
//            
//            $('#area').val(radius * 3.14 * radius);
//            
//            $('#result').html("Area of " + radius + " is " + (radius * 3.14 * radius))
//            
//            console.log(radius)          
//            calccircum(radius);
//        }
//        
        function calccircum(){
            
            var js_radius = $('#radii').val();
            
            $.ajax({
                
                url : "ajaxresponse.jsp",
                method : "GET",
                data : {"radius" : js_radius, "username" : "Amit"}
                
            }).success(function(data){
                //console.log(data) 
                $('#result').html(data);
            }).fail(function(){
                alert("error");
             }).always(function(){
                 alert("Finally & always");
             })
        }
        
    </script>
</html>
